'use strict';

/**
 * Cron config that gives you an opportunity
 * to run scheduled jobs.
 *
 * The cron format consists of:
 * [SECOND (optional)] [MINUTE] [HOUR] [DAY OF MONTH] [MONTH OF YEAR] [DAY OF WEEK]
 *
 * See more details here: https://strapi.io/documentation/developer-docs/latest/setup-deployment-guides/configurations.html#cron-tasks
 */

module.exports = {
  /**
   * Simple example.
   * Every monday at 1am.
   */
/*  '0 1 * * 1': async () => {
  // fetch posts to publish
  const draftPostToPublish = await strapi.api.services.post.find({
    status:'draft',
    published_at:new Date(),
  })
  // update status for posts
  draftPostToPublish.forEach(async () => {
await strapi.api.services.post.update({id:post.id},{status:'published'})
  });
   },*/
};
