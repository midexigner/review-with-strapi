module.exports = ({ env }) => ({
  host: env('HOST', '0.0.0.0'),
  port: env.int('PORT', 1337),
  /*cron:{
    enabled:true
  },*/
  admin: {
    auth: {
      secret: env('ADMIN_JWT_SECRET', '142913027af5a72bedddb502f4b3e8c2'),
    },
  },
});
